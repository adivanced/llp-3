#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "parser.tab.h"
#include "ast_tree.h"
#include "json.h"

	
#define SERVER_PORT 35103
#define SERVER_ADDR "127.0.0.1"

char recvbuf[4096];

int sockfd;

int main(void){
	struct sockaddr_in server_addr = (struct sockaddr_in){sin_family:AF_INET, sin_port:htons(SERVER_PORT), sin_addr:inet_addr(SERVER_ADDR)};
	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if(connect(sockfd, (struct sockaddr*)&server_addr, sizeof(struct sockaddr))){
		printf("Unable to connect to server!\n");
		//exit(0);
	}else{
		printf("Successfully connected to the server!\n");
	}

	for(;;){
		loop:;

		int yydebug = 0;
		yyparse();
		struct ast_tree tree = get_request_tree();
		print_request_tree(tree);
		if(tree.type == UNDEFINED){goto loop;}

		char* json = ast2json(&tree);

		printf("%s\n len:%ld\n", json, strlen(json));
		
		send(sockfd, json, strlen(json), 0);
		free(json);

		// pack to json

		// send to server
		free_tree(&tree);
		// wait for server answ

		recv(sockfd, recvbuf, 4096, 0);
		printf("%s\n", recvbuf);
	}


}